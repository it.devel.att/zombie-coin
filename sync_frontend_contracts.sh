#!/bin/bash

set -e

BASE_CONTRACTS_DIR="artifacts/contracts/"
FRONTEND_CONTRACTS_DIR="frontend/src/contracts/abi/"

# Add new contracts here
contracts="
	ZombieCoin.sol/ZombieCoin.json 
	ZombieCoinExchange.sol/ZombieCoinExchange.json
	"

for contract in $contracts
do
	from=${BASE_CONTRACTS_DIR}${contract}
	to=${FRONTEND_CONTRACTS_DIR}
	echo "cp ${from} ${to}"
	cp $from $to
done
echo "Done"
