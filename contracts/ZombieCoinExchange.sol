// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";


abstract contract ZombieCoinInterface is ERC20 {}


contract ZombieCoinExchange is Ownable {

    address private _zombieCoinContractAddress;
    bool public swapAvailable = false;
    address zombieCoinOwnerAddress; 

        // 1 ether = 1_000_000_000_000_000_000
    uint etherInUint = 10**18;
    uint rate = 10**15;

    ZombieCoinInterface zombieCoinContract;

    modifier isSwapAvailable() {
        require(swapAvailable, "Swap coins not available in current moment. Sorry......");
        _;
    }

    function swapEtherOnZombieCoin() public payable isSwapAvailable {
        uint amount = msg.value / rate;
        zombieCoinContract.transferFrom(zombieCoinOwnerAddress, msg.sender, amount);
    }

    function calculatedRequiredEther(uint zombieCoins) public view returns(uint) {
        return zombieCoins * rate;
    }

    // Owner only functions

    function toggleSwapAvailable() public onlyOwner {
        swapAvailable = !swapAvailable;
    }

    function getZombieCoinContractAddress() public view onlyOwner returns(address) {
        return _zombieCoinContractAddress;
    }

    function setZombieCoinContractAddress(address  _contractAddress) public onlyOwner {
        _zombieCoinContractAddress = _contractAddress;
        zombieCoinContract = ZombieCoinInterface(_zombieCoinContractAddress);
    }

    function getZombieCoinOwnerAddress() public view onlyOwner returns(address) {
        return zombieCoinOwnerAddress;
    }

    function setZombieCoinOwnerAddress(address  _address) public onlyOwner {
        zombieCoinOwnerAddress = _address;
    }

    function withdrawContractBalance() public onlyOwner {
        address payable wallet = payable(owner());
        wallet.transfer(_contractBalance());
    }

    function getContractBalance() public view onlyOwner returns(uint) {
        return _contractBalance();
    }

    function _contractBalance() internal view returns(uint) {
        return address(this).balance;
    }
}
