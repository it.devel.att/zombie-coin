// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract ZombieCoin is ERC20 {
    constructor() ERC20("Zombie Coin", "ZMC") {
        _mint(msg.sender, 100_000_000);
    }
}
