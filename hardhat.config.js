require("@nomiclabs/hardhat-waffle");

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log('Public address', account.address);
  }
});

task("approve-exchange", "Approve all zombie token balance to exchange contract", async (taskArgs, hre) => {
  const ZOMBIE_COIN_CONTRACT_ADDRESS = process.env.ZOMBIE_COIN_CONTRACT_ADDRESS;
  const ZOMBIE_COIN_EXCHANGE_CONTRACT_ADDRESS = process.env.ZOMBIE_COIN_EXCHANGE_CONTRACT_ADDRESS;
  console.log('ZOMBIE_COIN_CONTRACT_ADDRESS ', ZOMBIE_COIN_CONTRACT_ADDRESS);
  console.log('ZOMBIE_COIN_EXCHANGE_CONTRACT_ADDRESS ', ZOMBIE_COIN_EXCHANGE_CONTRACT_ADDRESS);

  const accounts = await hre.ethers.getSigners();
  const currentAccount = accounts[0];
  console.log('Current account address ', currentAccount.address);

  const zombieCoinContract = await (await hre.ethers.getContractFactory("ZombieCoin"))
      .attach(ZOMBIE_COIN_CONTRACT_ADDRESS);
  const zombieCoinExchangeContract = await (await hre.ethers.getContractFactory("ZombieCoinExchange"))
      .attach(ZOMBIE_COIN_EXCHANGE_CONTRACT_ADDRESS);

  const zombieCoinSymbol = await zombieCoinContract.symbol();


  const balance = await zombieCoinContract.balanceOf(currentAccount.address);
  await zombieCoinContract.approve(zombieCoinExchangeContract.address, balance);
  console.log(`Success approved zombie coin ${balance} to zombie coin exchange from ${currentAccount.address}`);
  await zombieCoinExchangeContract.setZombieCoinContractAddress(zombieCoinContract.address);

  await zombieCoinExchangeContract.setZombieCoinOwnerAddress(currentAccount.address);

  const exchangeZombieCoinAddress = await zombieCoinExchangeContract.getZombieCoinContractAddress();
  console.log(`Success set zombie coin address (${exchangeZombieCoinAddress}) to zombie coin exchange`);

  await zombieCoinExchangeContract.toggleSwapAvailable();
  let exchangeSwapAvailable = await zombieCoinExchangeContract.swapAvailable();
  if (!exchangeSwapAvailable) {
    await zombieCoinExchangeContract.toggleSwapAvailable();
    exchangeSwapAvailable = await zombieCoinExchangeContract.swapAvailable();
  }
  console.log(`Exchange swap available `, exchangeSwapAvailable);

  const exchangeApprovedCoins = await zombieCoinContract.allowance(currentAccount.address, zombieCoinExchangeContract.address);
  console.log(`exchangeApprovedCoins ${exchangeApprovedCoins.toString()} ${zombieCoinSymbol}`);

  // const secondAccount = accounts[1];
  // console.log('secondAccount.address ', secondAccount.address);

  // const secondAccountZombieCoinBalance = await zombieCoinContract.balanceOf(secondAccount.address);
  // const currentAccountZombieCoinBalance = await zombieCoinContract.balanceOf(currentAccount.address);
  // await zombieCoinExchangeContract.connect(secondAccount).swapEtherOnZombieCoin({ value: ethers.utils.parseEther("1") });
});


// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: "0.8.4",
  networks: {
    local: {
      url: `http://127.0.0.1:8545`,
    },
  },
};
