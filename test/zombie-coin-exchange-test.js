const { expect, assert } = require("chai");

describe("Token contract", () => {

  let zombieCoinContract;
  let zombieCoinExchangeContract;
  let owner;
  let addr1;
  let addr2;
  let addrs;
  let initialBalance = 100_000_000;
  let etherToZombieCointRate = 10**15;
  let etherInUint = 10**18;

  beforeEach(async () => {
    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

    const ZombieCoin = await ethers.getContractFactory("ZombieCoin");
    zombieCoinContract = await ZombieCoin.deploy();


    const ZombieCoinExchange = await ethers.getContractFactory("ZombieCoinExchange");
    zombieCoinExchangeContract = await ZombieCoinExchange.deploy();

    await zombieCoinExchangeContract.setZombieCoinContractAddress(zombieCoinContract.address);

    await zombieCoinExchangeContract.toggleSwapAvailable();
    expect(await zombieCoinExchangeContract.swapAvailable()).to.be.equal(true);

    const resultCoinContractAddress = await zombieCoinExchangeContract.getZombieCoinContractAddress();
    expect(resultCoinContractAddress).to.be.equal(zombieCoinContract.address);

    await zombieCoinExchangeContract.setZombieCoinOwnerAddress(owner.address);

    // Approve that this contract can transfer coins from owner address in zombie coin contract
    await zombieCoinContract.approve(zombieCoinExchangeContract.address, initialBalance);

    const result = await zombieCoinContract.allowance(owner.address, zombieCoinExchangeContract.address);
    expect(result.toNumber()).to.equal(initialBalance);
  });

  it("Swap ether on zombie coin and check balances", async () => {
    await zombieCoinExchangeContract.connect(addr1).swapEtherOnZombieCoin({ value: ethers.utils.parseEther("1") });

    const addr1ZombieCoinBalance = await zombieCoinContract.balanceOf(addr1.address);

    // 1000 zombie coins it's 1 ether
    const zombieCoinBalance = etherInUint / etherToZombieCointRate;
    expect(addr1ZombieCoinBalance).to.be.equal(zombieCoinBalance);

    const coinSwapBalance = await zombieCoinExchangeContract.getContractBalance();
    expect(coinSwapBalance).to.be.equal(ethers.utils.parseEther("1"));
  });

  it("Withdraw swap contract balance to owner", async () => {
    const balanceResult = await ethers.provider.getBalance(owner.address);
    const balanceBefore = balanceResult / etherInUint;


    await zombieCoinExchangeContract.connect(addr1).swapEtherOnZombieCoin({ value: ethers.utils.parseEther("1") });
    await zombieCoinExchangeContract.connect(addr1).swapEtherOnZombieCoin({ value: ethers.utils.parseEther("2") });
    await zombieCoinExchangeContract.connect(addr1).swapEtherOnZombieCoin({ value: ethers.utils.parseEther("3") });

    const addr1ZombieCoinBalance = await zombieCoinContract.balanceOf(addr1.address);

    // 1000 zombie coins it's 1 ether
    const zombieCoinBalance = (etherInUint * 6) / etherToZombieCointRate;
    expect(addr1ZombieCoinBalance).to.be.equal(zombieCoinBalance);

    const coinSwapBalance = await zombieCoinExchangeContract.getContractBalance();
    expect(coinSwapBalance).to.be.equal(ethers.utils.parseEther("6"));


    await zombieCoinExchangeContract.withdrawContractBalance();

    const newResult = await ethers.provider.getBalance(owner.address);
    const balanceAfter = newResult / etherInUint;

    expect(balanceBefore).to.be.not.equal(balanceAfter);
    expect(balanceAfter).to.be.greaterThan(balanceBefore);
  });
});