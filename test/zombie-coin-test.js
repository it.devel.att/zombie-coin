const { expect, assert } = require("chai");

describe("Token contract", () => {

  let contract;
  let owner;
  let addr1;
  let addr2;
  let addrs;
  let initialBalance = 100_000_000;

  beforeEach(async () => {
    const Token = await ethers.getContractFactory("ZombieCoin");
    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();
    contract = await Token.deploy();
  });

  it("Should deploy with coins to owner", async () => {
    const result = await contract.balanceOf(owner.address);

    expect(result.toNumber()).to.equal(initialBalance);
  });

  it("Should transfer from owner to addr1", async () => {
    await contract.transfer(addr1.address, 1000);

    const addrBalance = await contract.balanceOf(addr1.address);
    const ownerBalance = await contract.balanceOf(owner.address);


    expect(addrBalance.toNumber()).to.equal(1000);
    expect(ownerBalance.toNumber()).to.equal(initialBalance - 1000);
  });

  it("Should show total supply", async () => {
    const result = await contract.totalSupply();

    expect(result.toNumber()).to.equal(initialBalance);
  });

  it("Should show name", async () => {
    const result = await contract.name();

    expect(result).to.equal("Zombie Coin");
  });

  it("Should show symbol", async () => {
    const result = await contract.symbol();

    expect(result).to.equal("ZMC");
  });

  it("Should show decimals", async () => {
    const result = await contract.decimals();

    expect(result).to.equal(18);
  });

  it("Should approve and allowance check", async () => {
    await contract.approve(addr1.address, 100_000);

    const result = await contract.allowance(owner.address, addr1.address);
    expect(result.toNumber()).to.equal(100_000);
  });

  it("Should transfer from owner to addr2 through addr1", async () => {
    await contract.approve(addr1.address, 100_000);

    const ownerBalance = await contract.balanceOf(owner.address);
    expect(ownerBalance.toNumber()).to.equal(initialBalance);

    const result = await contract.allowance(owner.address, addr1.address);
    expect(result.toNumber()).to.equal(100_000);


    await contract.connect(addr1).transferFrom(owner.address, addr2.address, 90_000);

    const addr2Balance = await contract.balanceOf(addr2.address);
    expect(addr2Balance.toNumber(), 90_000)

    const newOwnerBalance = await contract.balanceOf(owner.address);
    expect(newOwnerBalance.toNumber()).to.equal(initialBalance - 90_000);

    const newAllowance = await contract.allowance(owner.address, addr1.address);
    expect(newAllowance.toNumber()).to.equal(10_000);
  });

  it("Cannot transfer from owner to addr2 through addr1 more then approve amount", async () => {
    await contract.approve(addr1.address, 10_000);

    const ownerBalance = await contract.balanceOf(owner.address);
    expect(ownerBalance.toNumber()).to.equal(initialBalance);

    const result = await contract.allowance(owner.address, addr1.address);
    expect(result.toNumber()).to.equal(10_000);


    await expect(contract.connect(addr1).transferFrom(owner.address, addr2.address, 10_001)).to.be.revertedWith('ERC20: transfer amount exceeds allowance');
  });

  it("Cannot transfer from not approved address", async () => {
    await contract.approve(addr1.address, 10_000);

    const result = await contract.allowance(owner.address, addr1.address);
    expect(result.toNumber()).to.equal(10_000);


    await expect(contract.connect(addr2).transferFrom(owner.address, addr2.address, 10_001)).to.be.revertedWith('ERC20: transfer amount exceeds allowance');
  });
});