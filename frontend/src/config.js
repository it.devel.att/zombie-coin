const config = {
    value: process.env.REACT_APP_SOME_VALUE || "default value",
    web3_provider: process.env.REACT_APP_WEB3_PROVIDER_URL || "http://127.0.0.1:8545/",
    zombieCoinContractAddress: process.env.REACT_APP_ZOMBIE_COIN_CONTRACT_ADDRESS,
    zombieCoinExchangeContractAddress: process.env.REACT_APP_ZOMBIE_COIN_EXCHANGE_CONTRACT_ADDRESS,
};

console.log('config ', config);

export default config;