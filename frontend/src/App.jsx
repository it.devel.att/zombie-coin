import './App.css';
import Main from './components/content/Main';
import Web3 from 'web3';
import config from './config';

function App() {

  function checkWeb3Injected() {
    let provider;
    if (window.ethereum) {
      provider = window.ethereum;
    } else if (window.web3) {
      provider = window.web3.currentProvider;
    } else {
      window.alert("No ethereum browser detected!")
    }
    console.log('provider', provider);
    const web3 = new Web3(provider || config.web3_provider);
    return <Main web3={web3}/>
  }

  return (
    <div className="App">
      {checkWeb3Injected()}
    </div>
  );
}

export default App;
