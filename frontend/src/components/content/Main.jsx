import React, {useEffect, useState} from 'react';
import Header from '../header/Header';
import zombieCoinContractABI from '../../contracts/abi/ZombieCoin.json'
import zombieCoinContractExchangeABI from '../../contracts/abi/ZombieCoinExchange.json'
import config from '../../config';
import ZombieCoinInterface from "../../contracts/interfaces/ZombieCoin";
import ZombieCoinExchangeInterface from "../../contracts/interfaces/ZombieCoinExchange";
import weiToEther from "../../contracts/utils/utils";
import TextField from "@mui/material/TextField";
import Alert from "@mui/material/Alert";
import Container from "@mui/material/Container";
import Snackbar from "@mui/material/Snackbar";

function Main(props) {
    const [account, setAccount] = useState(null);
    const [zombieCoinBalance, setZombieCoinBalance] = useState(0);
    const [etherBalance, setEtherBalance] = useState('');

    const [zombieCoinsInput, setZombieCoinsInput] = useState(0);
    const [etherRequired, setEtherRequired] = useState('');

    const [open, setOpen] = useState(false);

    const zombieCoinContract = new ZombieCoinInterface(props.web3, zombieCoinContractABI.abi, config.zombieCoinContractAddress);
    const zombieCoinExchangeContract = new ZombieCoinExchangeInterface(props.web3, zombieCoinContractExchangeABI.abi, config.zombieCoinExchangeContractAddress);

    useEffect(async () => {
        const accounts = await props.web3.eth.getAccounts();
        console.log('accounts', accounts);
        const defaultAccount = accounts[0];


        if (window.ethereum) {
            window.ethereum.on('accountsChanged', function (accounts) {
                // Time to reload your interface with accounts[0]!
                console.log('accountsChanged', accounts[0]);
                setAccount(accounts[0]);
                refreshDependsOnAccount(defaultAccount);
            });
        }

        setAccount(defaultAccount);
        refreshDependsOnAccount(defaultAccount);
    }, [account]);

    async function refreshDependsOnAccount(account) {
        const balance = await zombieCoinContract.balanceOf(account);
        setZombieCoinBalance(balance);

        const etherBalance = await props.web3.eth.getBalance(account);
        console.log(typeof weiToEther(etherBalance));
        setEtherBalance(weiToEther(etherBalance));
    }

    async function buyZombieCoin(e) {
        e.preventDefault();

        const etherForCoins = await zombieCoinExchangeContract.calculatedRequiredEther(zombieCoinsInput);
        await zombieCoinExchangeContract.swapZombieCoin(account, etherForCoins);

        const balance = await zombieCoinContract.balanceOf(account);
        setZombieCoinBalance(balance);
        setZombieCoinsInput(0);
        setOpen(true);
    }


    async function calculateEthRequired(coins) {
        if (!coins) {
            setZombieCoinsInput(coins);
            return
        }
        const resultWei = await zombieCoinExchangeContract.calculatedRequiredEther(coins);
        console.log(weiToEther(resultWei));
        setZombieCoinsInput(coins);
        setEtherRequired(weiToEther(resultWei));
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    return (
        <div>
            <Header
                account={account}
                zombieCoinBalance={zombieCoinBalance}
                etherBalance={etherBalance}
            />
            <Container maxWidth="sm">
                <Alert sx={{m: 2}} severity="success">Exchange zombie coins</Alert>
                <form onSubmit={buyZombieCoin}>
                    <TextField
                        id="outlined-number"
                        label="Amount of ZMC"
                        type="number"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={(e) => calculateEthRequired(e.target.value)}
                    />
                    <Alert sx={{m: 2}} severity="success">{zombieCoinsInput} ZYC = {etherRequired} ether</Alert>
                </form>
                <div>
                </div>
            </Container>
            <Snackbar
                open={open}
                autoHideDuration={6000}
                onClose={handleClose}
                anchorOrigin={{vertical: 'top', horizontal: 'right'}}
            >
                <Alert onClose={handleClose} severity="success" sx={{width: '100%'}}>
                    Success convert!
                </Alert>
            </Snackbar>
        </div>
    )
}

export default Main;