import { useEffect, useState } from "react";
import config from "../../config";


function Header(props) {

    return (
        <div>
            <div>Current account address: {props.account ? props.account : 'account address not found'}</div>
            <div>ZombieCoin balance: {props.zombieCoinBalance ? props.zombieCoinBalance : '0'} coins</div>
            <div>Ether balance: {props.etherBalance ? props.etherBalance : '0'} ether</div>
        </div>
    );
}

export default Header;