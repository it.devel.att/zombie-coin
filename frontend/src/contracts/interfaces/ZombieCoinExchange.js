class ZombieCoinExchangeInterface {
    provider;
    contract;

    constructor(provider, abi, address) {
        this.provider = provider;
        this.contract = new this.provider.eth.Contract(abi, address);
    }

    async swapZombieCoin(address, ethValue) {
        return await this.contract.methods.swapEtherOnZombieCoin().send({from: address, value: ethValue});
    }

    async calculatedRequiredEther(zombieCoins) {
        return await this.contract.methods.calculatedRequiredEther(zombieCoins).call();
    }

    // TODO Implement contract methods (wrap)
}

export default ZombieCoinExchangeInterface;