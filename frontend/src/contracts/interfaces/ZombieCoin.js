class ZombieCoinInterface {
    provider;
    contract;

    constructor(provider, abi, address) {
        this.provider = provider;
        this.contract = new this.provider.eth.Contract(abi, address);
    }
    // TODO Implement contract methods (wrap)

    async balanceOf(address) {
        return await this.contract.methods.balanceOf(address).call()
    }
}

export default ZombieCoinInterface;