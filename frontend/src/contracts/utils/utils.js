import Web3 from 'web3';


function weiToEther(wei) {
    const etherValue = Web3.utils.fromWei(wei, 'ether');
    console.log(etherValue);
    return etherValue;
}

export default weiToEther;